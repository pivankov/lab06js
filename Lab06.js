describe('Hunde-Tracker Test'), () => {
    beforeEach(() => {
cy.visit('https://www.telekom.de/start')
    })

it('displays the needed menu element', () => {
        cy.get("//a[@aria-label='Smarte Produkte']").trigger('mouseover')
        cy.get("//a[@aria-label='Smarte Produkte']").click({ force: true })
        cy.get("//a[@aria-label='Hunde Tracker']").trigger('mouseover')
        cy.get("//a[@aria-label='Hunde Tracker']").click({ force: true })
        cy.get("//h1[@aria-label='Hunde Tracker']")
        .should('have.text', 'Der GPS-Tracker für Hunde')
        .should('not.have.text', 'Pay electric bill')
        cy.get("//*[@href='/unterwegs/alcatel/alcatel-combi-protect/schwarz-16mb?tariffId=15332']")
        .should("be.visible")
})
}
